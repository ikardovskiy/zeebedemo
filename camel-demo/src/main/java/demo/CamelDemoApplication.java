package demo;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class CamelDemoApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CamelDemoApplication.class)
                .build()
                .run(args);
    }

    @Component
    public static class Routes extends RouteBuilder {
        @Override
        public void configure() {
            from("ftp:{{ftp.user}}@{{ftp.host}}/{{ftp.dir.source}}?stepwise=false&passiveMode=true&password={{ftp.password}}&delete=true")
                    .errorHandler(deadLetterChannel("ftp:{{ftp.user}}@{{ftp.host}}/{{ftp.dir.error}}?passiveMode=true&password={{ftp.password}}"))
                    .to("xslt:xslt/transform.xsl")
                    .setProperty("arg1", xpath("/target/a/text()", Long.class))
                    .setProperty("arg2", xpath("/target/b/text()", Long.class))
                    .setProperty("sum").groovy("exchange.properties.arg1 + exchange.properties.arg2")
                    .to("xslt:xslt/addSum.xsl?contentCache=true")
                    .to("ftp:{{ftp.user}}@{{ftp.host}}/{{ftp.dir.dest}}?passiveMode=true&password={{ftp.password}}");
        }
    }
}
