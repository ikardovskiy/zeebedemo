package demo;

import org.apache.camel.builder.endpoint.EndpointRouteBuilder;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class CamelDemoEndpointDSLApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CamelDemoEndpointDSLApplication.class)
                .build()
                .run(args);
    }

    @Component
    public static class Routes extends EndpointRouteBuilder {
        @Override
        public void configure() {
            from(ftp("{{ftp.user}}@{{ftp.host}}/{{ftp.dir.source}}")
                    .passiveMode(true)
                    .password("{{ftp.password}}")
                    .delete(true)
                    .advanced().stepwise(false))
                    .routeId("camel-demo")
                    .errorHandler(deadLetterChannel(ftp("{{ftp.user}}@{{ftp.host}}/{{ftp.dir.error}}")
                            .passiveMode(true)
                            .password("{{ftp.password}}")
                            .advanced().stepwise(false).getUri()))
                    .to(xslt("xslt/transform.xsl")).id("transform")
                    .setProperty("arg1", xpath("/target/a/text()", Long.class))
                    .setProperty("arg2", xpath("/target/b/text()", Long.class))
                    .setProperty("sum").groovy("exchange.properties.arg1 + exchange.properties.arg2")
                    .to(xslt("xslt/addSum.xsl?contentCache=true")).id("add-sum")
                    .to(ftp("{{ftp.user}}@{{ftp.host}}/{{ftp.dir.dest}}")
                            .passiveMode(true)
                            .password("{{ftp.password}}"))
                    .id("ftp-destination");
        }
    }
}
