package demo;

import io.zeebe.client.ZeebeClient;
import io.zeebe.client.api.response.ActivatedJob;
import io.zeebe.client.api.response.WorkflowInstanceEvent;
import io.zeebe.client.api.worker.JobClient;
import io.zeebe.spring.client.EnableZeebeClient;
import io.zeebe.spring.client.annotation.ZeebeDeployment;
import io.zeebe.spring.client.annotation.ZeebeWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableZeebeClient
@ZeebeDeployment(classPathResources = "demo_process.bpmn")
@Slf4j
public class ZeebeDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZeebeDemoApplication.class, args);
    }

    @Component
    @ConditionalOnProperty(prefix = "worker", name = "transform", havingValue = "true")
    public static class Transformer {

        @Autowired
        private ProducerTemplate producerTemplate;

        @ZeebeWorker(type = "transform-message")
        public void transform(final JobClient client, final ActivatedJob job) {
            log.trace("Transform: {}", job);
            Map<String, Object> variables = job.getVariablesAsMap();
            try {
                String body = (String) variables.get("body");
                Exchange e = producerTemplate.request("direct:transform",
                        exchange -> exchange.getIn().setBody(body));
                if (e.isFailed()) {
                    throw e.getException();
                }
                var result = e.getOut().getBody(String.class);
                variables.put("body", result);
                client.newCompleteCommand(job.getKey())
                        .variables(variables)
                        .send()
                        .join();
            } catch (Exception e) {
                log.error("Error tranforming", e);
                client.newFailCommand(job.getKey())
                        .retries(-1)
                        .errorMessage(e.getMessage())
                        .send()
                        .join();
            }
        }
    }


    @Component
    @ConditionalOnProperty(prefix = "worker", name = "publish", havingValue = "true")
    public static class Publisher {

        @Autowired
        private ProducerTemplate producerTemplate;

        @ZeebeWorker(type = "publish-message", name = "publish-message")
        public void publish(final JobClient client, final ActivatedJob job) {
            log.trace("Publish: {}", job);
            Map<String, Object> variables = job.getVariablesAsMap();
            try {
                String body = (String) variables.get("body");
                String file = (String) variables.get("file");
                Exchange e = producerTemplate.request("direct:publish",
                        exchange -> {
                            exchange.getIn().setBody(body);
                            exchange.getIn().setHeader(Exchange.FILE_NAME, file);
                        }
                );
                if (e.isFailed()) {
                    throw e.getException();
                }
                var result = e.getIn().getBody(String.class);
                variables.put("body", result);
                client.newCompleteCommand(job.getKey())
                        .variables(variables)
                        .send()
                        .join();
            } catch (Exception e) {
                log.error("Error publishing,", e);
                client.newFailCommand(job.getKey())
                        .retries(-1)
                        .errorMessage(e.getMessage())
                        .send()
                        .join();
            }
        }
    }

    @Component
    @ConditionalOnProperty(prefix = "worker", name = "calculate-sum", havingValue = "true")
    public static class SumCalculator {

        @Autowired
        private ProducerTemplate producerTemplate;

        @ZeebeWorker(type = "calculate-sum", name = "calculate-sum")
        public void calculateSum(final JobClient client, final ActivatedJob job) {
            log.trace("Calculate sum: {}", job);
            Map<String, Object> variables = job.getVariablesAsMap();
            try {
                String body = (String) variables.get("body");
                Exchange e = producerTemplate.request("direct:calculate-sum",
                        exchange -> {
                            exchange.getIn().setBody(body);
                        });
                if (e.isFailed()) {
                    throw e.getException();
                }
                var arg1 = e.getProperty("arg1", Long.class);
                var arg2 = e.getProperty("arg2", Long.class);
                var sum = e.getProperty("sum", String.class);

                variables.put("arg1", arg1);
                variables.put("arg2", arg2);
                variables.put("sum", sum);

                client.newCompleteCommand(job.getKey())
                        .variables(variables)
                        .send()
                        .join();
            } catch (Exception e) {
                log.error("Error calculate sum,", e);
                client.newFailCommand(job.getKey())
                        .retries(-1)
                        .errorMessage(e.getMessage())
                        .send()
                        .join();
            }
        }
    }

    @Component
    @ConditionalOnProperty(prefix = "worker", name = "add-sum-info", havingValue = "true")
    public static class SumInfoAdder {

        @Autowired
        private ProducerTemplate producerTemplate;

        @ZeebeWorker(type = "add-sum-info", name = "add-sum-info")
        public void addSumInfo(final JobClient client, final ActivatedJob job) {
            log.trace("Add sum info: {}", job);
            Map<String, Object> variables = job.getVariablesAsMap();
            try {
                String body = (String) variables.get("body");
                String sum = (String) variables.get("sum");
                Exchange e = producerTemplate.request("direct:add-sum-info",
                        exchange -> {
                            exchange.getIn().setBody(body);
                            exchange.getIn().setHeader("sum", sum);
                        });
                if (e.isFailed()) {
                    throw e.getException();
                }
                var result = e.getOut().getBody(String.class);
                variables.put("body", result);

                client.newCompleteCommand(job.getKey())
                        .variables(variables)
                        .send()
                        .join();
            } catch (Exception e) {
                log.error("Error calculate sum,", e);
                client.newFailCommand(job.getKey())
                        .retries(-1)
                        .errorMessage(e.getMessage())
                        .send()
                        .join();
            }
        }
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Component
    public static class WorkerRoutes extends RouteBuilder {

        @Autowired
        private ZeebeClient client;

        @Override
        public void configure() {
//            getContext().setTracing(true);
//            DefaultExchangeFormatter formatter = new DefaultExchangeFormatter();
//            formatter.setShowAll(true);
//            getContext().getTracer().setExchangeFormatter(formatter);

            from("ftp:{{ftp.user}}@{{ftp.host}}/{{ftp.dir.source}}?stepwise=false&passiveMode=true&password={{ftp.password}}&delete=true")
                    .routeId("process starter")
                    .process(exchange -> {
                        Map<String, Object> variables = new HashMap<>();
                        variables.put("file", exchange.getIn().getHeader("CamelFileName", String.class));
                        variables.put("body", exchange.getIn().getBody(String.class));
                        final WorkflowInstanceEvent wfInstance = client.newCreateInstanceCommand()
                                .bpmnProcessId("demo_process")
                                .latestVersion()
                                .variables(variables)
                                .send()
                                .join();
                        final long workflowInstanceKey = wfInstance.getWorkflowInstanceKey();

                        log.trace("Workflow instance created. Key: {}", workflowInstanceKey);
                    });

            from("direct:calculate-sum")
                    .routeId("calculate-sum")
                    .setProperty("arg1", xpath("/target/a/text()", Long.class))
                    .setProperty("arg2", xpath("/target/b/text()", Long.class))
                    .setProperty("sum").groovy("exchange.properties.arg1 + exchange.properties.arg2");

            from("direct:add-sum-info")
                    .routeId("add-sum-info")
                    .to("xslt:xslt/addSum.xsl");

            from("direct:transform")
                    .routeId("transform")
                    .to("xslt:xslt/transform.xsl");
            from("direct:publish")
                    .routeId("publish")
                    .to("ftp:{{ftp.user}}@{{ftp.host}}/dst?passiveMode=true&password={{ftp.password}}");
        }
    }
}
