package demo;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.ProducerTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import static java.lang.String.format;

@SpringBootApplication
public class CamelUploaderApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CamelUploaderApplication.class)
                .build()
                .run(args);
    }


    @Bean
    public CommandLineRunner getRunner(ProducerTemplate producerTemplate,
                                       CamelContext context,
                                       ApplicationContext applicationContext) {
        return args -> {
            Endpoint endpoint = context.getEndpoint("ftp:{{ftp.user}}@{{ftp.host}}/src?passiveMode=true&password={{ftp.password}}");
            String template = "<source a=\"arg1\" b=\"arg2\" />";

            for (int i = 0; i < 1000; i++) {
                final int j = i;
                producerTemplate.send(endpoint,
                        exchange -> {
                            var message = template.replace("arg1", String.valueOf(j))
                                    .replace("arg2", String.valueOf(j + 1));
                            exchange.getIn()
                                    .setBody(message);
                            exchange.getIn()
                                    .setHeader("CamelFileName", format("finaltestfile%s.txt", j));
                        }
                );
            }

            SpringApplication.exit(applicationContext);

            System.exit(0);

        };
    }
}
